Raspberry Pi Kernel
===================

A work in progress bare-metal kernel for the Raspberry Pi

To Build:
---------
Type `make`

The kernel image will be generated in the ./bin directory.