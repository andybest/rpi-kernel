# Makefile/project structure based on https://github.com/MichaelBell/RasPi-bare

# Compiler setup
ARCH = arm-none-eabi

CC = $(ARCH)-gcc
CPP = $(ARCH)-g++
AS = $(ARCH)-as
LD = $(ARCH)-ld
AR = $(ARCH)-ar
OBJCOPY = $(ARCH)-objcopy

PLATFORM = raspi

CFLAGS = -O0 -g -std=gnu99 -Wall -Wextra -Werror -D__$(PLATFORM)__ -DRASPBERRY_PI -fno-builtin
ASFLAGS = -g

CFLAGS_FOR_TARGET = -mcpu=arm1176jzf-s
ASFLAGS_FOR_TARGET = -mcpu=arm1176jzf-s
LDFLAGS = -nostdlib -static --error-unresolved-symbols

# Source setup
MODULES := boot test init hardware
SRC_DIR := $(addprefix src/,$(MODULES))
INC_DIR := $(addsuffix /include,$(SRC_DIR))
BUILD_DIR := $(addsuffix /build,$(SRC_DIR))

# Assembler sources
ASRC := $(foreach sdir$,$(SRC_DIR),$(wildcard $(sdir)/*.s))
AOBJ := $(ASRC:.s=.o)

# C sources
CSRC := $(foreach sdir$,$(SRC_DIR),$(wildcard $(sdir)/*.c))
COBJ := $(CSRC:.c=.o)

INCLUDES := -Isrc/include -Isrc $(addprefix -I,$(SRC_DIR) $(INC_DIR))

# Generate vpaths for the source files
vpath %.c $(SRC_DIR)
vpath %.cpp $(SRC_DIR)
vpath %.s $(SRC_DIR)


# Rules

%.o: %.c
	@echo [CC] $@
	@$(CC) $(CFLAGS_FOR_TARGET) $(INCLUDES) $(CFLAGS) -c -o $*.o $<
	
%.o: %.s
	@echo [AS] $@
	@$(AS) $(ASFLAGS_FOR_TARGET) $(INCLUDES) $(ASFLAGS) -o $*.o $<
	
OBJ = $(AOBJ) $(COBJ)


bin/kernel.img: bin/kernel.elf
	@echo [Building kernel.img]
	$(OBJCOPY) -O binary $< $@

bin/kernel.elf: raspi.ld $(OBJ)
	@echo [Linking...]
	$(LD) $(LDFLAGS) $(OBJ) -Map bin/kernel.map -o $@ -T raspi.ld
	
clean:
	rm -f bin/*.elf bin/*.img bin/*.map $(OBJ)
