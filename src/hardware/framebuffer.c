#include "framebuffer.h"
#include "font_8x8.h"

void fb_initFramebuffer()
{
	fbInfo = (volatile struct PIFramebufferInfo *)fbmain();
	// Cast framebuffer address to an unsigned short, since we are using a 16 bit framebuffer.
	fb_pixelsBase = (unsigned short *)fbInfo->pointer;
}

void fb_writeCharToScreen(char ch, int x, int y)
{
	if (x > 1024 || y > 768)
		return;
	
	int x_min = 8 * x;
	int x_max = x_min + 8;
		
	int y_min = 8 * y;
	int y_max = y_min + 8;
		
	for(int y = y_min; y < y_max; y++)
	{
		for(int x = x_min; x < x_max; x++)
		{
			int currentPixel = x + (y * 1024);
				
			int character_row = y - y_min;
			int character_column_shift = -((x - x_min) - 8);
				
			char character_pixel = font_8x8[(unsigned char)ch][character_row];
			character_pixel = (character_pixel >> character_column_shift) & 0x1;
				
			fb_pixelsBase[currentPixel] = (character_pixel> 0)? 0x7E0: 0x0;
		}
	}
}

void fb_scrollLine()
{
	for(int i = 128; i < 128 * 96; i++)
	{	
		fb_textBuffer[i - 128] = fb_textBuffer[i];
	}
	
	for(int i = 128 * 96 - 128; i < 128 * 96; i++)
	{
		fb_textBuffer[i] = 0;
	}
	
	fb_updateWholeDisplay();
}

void fb_setCursor(int x, int y)
{
	fb_cursorX = x;
	fb_cursorY = y;
}

void fb_plotChar(char ch, int x, int y)
{
	fb_textBuffer[x + (y * 128)] = ch;
	fb_updateChar(x, y);
}

void fb_putChar(char ch)
{
	if(ch == '\n')
	{
		fb_cursorX = 0;
		fb_updateLine(fb_cursorY);
		fb_cursorY++;
		if(fb_cursorY > 95)
		{
			fb_scrollLine();
			fb_cursorY = 95;
		}
		return;
	}
	
	fb_plotChar(ch, fb_cursorX, fb_cursorY);
	
	fb_cursorX++;
	
	if(fb_cursorX > 127)
	{
		fb_cursorX = 0;
		fb_cursorY++;
		if(fb_cursorY > 95)
		{
			fb_scrollLine();
			fb_cursorY = 95;
		}
	}
}

void fb_putString(char *str)
{	
	while(*str != '\0')
	{
		fb_putChar(*str);
		str++;
	}
}

void fb_updateWholeDisplay()
{
	for(int x = 0; x < 128; x++)
	{
		for(int y = 0; y < 96; y++)
		{
			fb_writeCharToScreen(fb_textBuffer[x + (y * 128)], x, y);
		}
	}
}

void fb_updateChar(int x, int y)
{
	fb_writeCharToScreen(fb_textBuffer[x + (y * 128)], x, y);
}

void fb_updateLine(int y)
{
	for(int x = 0; x < 127; x++)
	{
		fb_updateChar(x, y);
	}
}