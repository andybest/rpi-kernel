#pragma once

extern unsigned int rpi_MailboxRead(unsigned int channel);
extern void rpi_MailboxWrite(unsigned int channel, unsigned int data);
