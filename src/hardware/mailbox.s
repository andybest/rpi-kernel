.arm
.text


# Load the base address of mailbox 0
.global rpi_GetMailboxBase
rpi_GetMailboxBase:
    ldr r0,=0x2000B880
    mov pc,lr


# Read from the mailbox. 
# Arguments:
#   channel (unsigned int)
# Returns:
#   data (unsigned int)
.global rpi_MailboxRead
rpi_MailboxRead:
    # Save register state
    stmdb sp!, {r4-r11}
    
    # Make sure that the channel number passed in is <  4 bits long
    # If it is longer, return with a zero.
    cmp r0, #15
    movhi r0, #0
    movhi pc, lr
    
    channel .req r1
    mov channel, r0

    # Save the return address, since we are going to call a function
    push {lr}

    # Load the mailbox base address
    bl rpi_GetMailboxBase
    mailbox .req r0

rightChannel:
waitForMessage:
    # Loop until the mailbox status register indicates that there is a message
    # waiting for collection
    status .req r2
    ldr status,[mailbox, #0x18]
    tst status,#0x40000000
    .unreq status
    bne waitForMessage

    # Read the mail from the mailbox
    mail .req r2
    ldr mail,[mailbox, #0]

    # Isolate the channel from the message
    readchan .req r3
    and readchan, mail, #0b1111

    # Check that the message is from the desired channel
    teq readchan, channel
    .unreq readchan
    bne rightChannel
    .unreq mailbox
    .unreq channel

    # Move the mail to the return register
    and r0, mail, #0xfffffff0
    .unreq mail

    # Restore register state
    ldmia sp!, {r4-r11}
    # Pop lr back into pc
    pop {pc}


.global rpi_MailboxWrite
rpi_MailboxWrite:
    # Save register state
    stmdb sp!, {r4-r11}
    
    # Test that data has the least significant 4 bytes set to 0
    tst r1, #0b1111
    movne pc, lr

    # Test that the channel is less than or equal to 15
    cmp r0, #15
    movhi pc, lr

    channel .req r2
    value .req r1
    
    mov channel, r0
    push {lr}

    bl rpi_GetMailboxBase
    mailbox .req r0

    # Wait until there is space in the mailbox
waitForSpace:
    status .req r3
    ldr status, [mailbox, #0x18]
    tst status,#0x80000000
    .unreq status
    bne waitForSpace
    
    # Combine the value and channel, then write it to the mailbox
    add value, channel
    .unreq channel
    str value, [mailbox, #0x20]
    .unreq value
    .unreq mailbox

    ldmia sp!, {r4-r11}
    pop {pc}
