#pragma once

typedef struct
{
    void (*init)(void *self);
    void (*destroy)();
    char *name;
} DriverDef_t;
