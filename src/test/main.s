.section .text

/*
* main is what we shall call our main operating system method. It never 
* returns, and takes no parameters.
* C++ Signature: void main()
*/
.global fbmain
fbmain:

/*
* Setup the screen.
*/
	push {lr}
	mov r0,#1024
	mov r1,#768
	mov r2,#16
	bl InitialiseFrameBuffer

/*
* Check for a failed frame buffer.
*/
	teq r0,#0
	bne noError$
		
	mov r0,#16
	mov r1,#1
	bl SetGpioFunction

	mov r0,#16
	mov r1,#0
	bl SetGpio

	error$:
		b error$

	noError$:

	fbInfoAddr .req r4
	mov fbInfoAddr,r0

	#fbAddr .req r3
	#ldr fbAddr,[fbInfoAddr,#32]
	#mov r0, fbInfoAddr
	pop {pc}
	
	.unreq fbInfoAddr
