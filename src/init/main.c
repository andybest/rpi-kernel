#include "framebuffer.h"

// C entry point
void k_main(void)
{	
	// Temporary framebuffer for debug info.
	fb_initFramebuffer();
	
	fb_putString("Hello, World! This is a test of my text rendering system for Raspberry Pi!\n");
	fb_putString("I hope this newline works correctly!\n");
	fb_putString("\n\n\nYay!!!!!!!!!!!!");
	fb_putString("\nThis should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... This should be wrapped... \n");
	
	int num = 0;
	while(1)
	{
		char *str = "This is a test-  \n";
		str[15] = num + 48;
		fb_putString(str);
		num++;
		num %= 10;
		//fb_updateDisplay();
		
		for(int i=0; i<999999; i++){}
	}
	
	while(1){}
}
