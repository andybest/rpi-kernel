#include "mm.h"

#include <stdint.h>

#define k_mm_baseAddressMask 0xFFF


void mm_init()
{
    // Init the L1 TLB
    uint32_t *tlb_level1_base = (uint32_t *)mm_init_tlb_l1();
    
    // Fill the TLB with default values
    for(uint32_t i = 0; i< 4096; i++)
    {
        uint32_t entry = mm_generateSectionEntry(i, 0x3, 0x0);
        tlb_level1_base[i] = entry;
    }
}


uint32_t mm_generateSectionEntry(
        uint32_t base_address, 
        unsigned char access_permissions,
        unsigned char domain)
{
    uint32_t section_entry = 0;
    
    section_entry = base_address & k_mm_baseAddressMask;
    section_entry |= (access_permissions & 0x3) << 10;
    section_entry |= (domain & 0xF) << 4;
    
    // Least significant 2 bits are always 0b10
    section_entry |= 0x2;
    
    return section_entry;
}
