.arm
.section text

.global mm_init_tlb_l1
mm_init_tlb_l1:
    # Save register state
    stmdb sp!, {r4-r11}
    
    ldr r0, =tlb_l1_base
    # Tell TLB where the base of our table is
    mcr p15, 0, r0, c2, c0, 0

    # Restore register state
    ldmia sp!, {r4-r11}
    # Set the L1 tlb base address as the return value
    ldr r0, =tlb_l1_base
    mov pc, lr


.section bss
# TLB level 1 must be on a 16kb boundary
.balign 16384
tlb_l1_base:
.space 16384


.end
