.global exc_stack
.global supervisor_sp
.globl resetFunc
	
.section .init
resetFunc:
		bl _start
			
.section .text
.globl	_start
# Entry point for the kernel
_start:

    /*
     * Set up exception vectors
     */

	# Init stack pointer for exception modes
	mrs r4, cpsr
    # Clear mode bits in CPSR register
	bic r4, r4, #0x1f

    # FIQ mode
    # Enable FIQ mode bits and push into CPSR register
    orr r3, r4, #0x11
    msr cpsr_c, r3
    ldr sp, =exc_stack

    # IRQ Mode
    # Enable IRQ mode bits and push into CPSR register
    orr r3, r4, #0x12
    msr cpsr_c, r3
    ldr sp, =exc_stack

    # Abort Mode
    # Enable Abort mode bits and push into CPSR register
    orr r3, r4, #0x17
    msr cpsr_c, r3
    ldr sp, =exc_stack
    # Store interrupt vectors pointing to  AbortInterrupt routine
    mov r0, #0x0000
    ldr r1, =AbortInterrupt
    # Point Prefetch Abort and Data Abort to AbortInterrupt routine
    str r1, [r0, #0x0010]
    str r1, [r0, #0x000C]
    
    # Undefined Mode
    # Enable Undefined mode bits and push into CPSR register
    orr r3, r4, #0x1b
    msr cpsr_c, r3
    ldr sp, =exc_stack
    # Store interrupt vectors pointing to UndefinedInterrupt routine
    ldr r1, =UndefInterrupt
    str r1, [r0, #0x0004]

    # Switch to supervisor mode
    orr r3, r4, #0x13
    msr cpsr_c, r3
    # Switch to temporary stack while we set up the first stack frame
    ldr sp, =temp_stack

    # Create first stack frame
    mov fp, #0x00
    mov ip, sp
    push {fp, ip, lr, pc}
    sub fp, ip, #4
	
	mov sp,#0x8000

    # Jump to C code entry point
    bl k_main
    
# Loop just in case k_main returns
KReturnLoop:
    b KReturnLoop


UndefInterrupt:
    #bl undefinedError
    mov pc, lr

AbortInterrupt:
    #bl abortError
    mov pc, lr

.globl PUT32
PUT32:
    str r1,[r0]
    bx lr

.globl GET32
GET32:
    ldr r0,[r0]
    bx lr

.globl dummy
dummy:
    bx lr

# Allocate some space (1k) for the stack
.space 0x100
temp_stack:
.space 1024
.global exc_stack
exc_stack:

# Stack pointer for supervisor mode
.global supervisor_sp
supervisor_sp:
.space 4

.bss

# We need to allocate some memory for the framebuffer info struct, 
# since it needs to be aligned on a 4 byte boundary.
.p2align 4
.global FramebufferMemory
FramebufferMemory:
.space 0x400

.global EndFrameBuffer
EndFrameBuffer:
.space 4

.global LinuxMachineType
LinuxMachineType:
.word 00000000

.global AtagsAddress
AtagsAddress:
.word 00000000
	
.end
