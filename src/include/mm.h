#pragma once

#include <stdint.h>

void mm_init();
void* mm_init_tlb_l1();
inline uint32_t mm_generateSectionEntry(
        uint32_t base_address, 
        unsigned char access_permissions,
        unsigned char domain);
