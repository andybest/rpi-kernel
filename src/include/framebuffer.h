#pragma once

#include <stdint.h>

extern unsigned short* fbmain();
extern unsigned int FrameBufferInfo;
extern void InitialiseFrameBuffer(unsigned int width, unsigned int height, unsigned int bitDepth);
extern unsigned char rpi_MailboxRead(unsigned char channel);
extern void rpi_MailboxWrite(unsigned int value, unsigned char channel);

unsigned short *fb_pixelsBase;
volatile struct PIFramebufferInfo *fbInfo;

char fb_textBuffer[128*96];
int fb_cursorX;
int fb_cursorY;

void fb_initFramebuffer();
void fb_writeCharToScreen(char ch, int x, int y);
void fb_scrollLine();
void fb_setCursor(int x, int y);
void fb_plotChar(char ch, int x, int y);
void fb_putChar(char ch);
void fb_putString(char *str);
void fb_updateDisplay();
void fb_updateWholeDisplay();
void fb_updateChar(int x, int y);
void fb_updateLine(int y);

//extern unsigned short *framebufferBase;

struct PIFramebufferInfo
{
   unsigned int width;
   unsigned int height;
   unsigned int virtual_width;
   unsigned int virtual_height;
   unsigned int pitch;              // Set by the GPU
   unsigned int depth;
   unsigned int x_offset;
   unsigned int y_offset;
   unsigned int *pointer;           // Set by the GPU
   unsigned int size;               // Set by the GPU
};


