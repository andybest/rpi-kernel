#pragma once

#include "driver.h"

typedef struct {
    DriverDef_t driver;
    void (*print_char)(char ch);
    void (*print_buffer)(char *buf);
} ttyRef_t;

ttyRef_t* d_ttyDriver_init()
{
    // Allocate a ttyDriver and populate it with generic data
    ttyRef_t ttyDriverRef = kmalloc(sizeof(ttyRef_t));
    ttyDriverRef->driver.name = "Generic TTY Driver";
    ttyDriverRef->driver.print_char = &d_ttyDriver_print_char_stub;
    ttyDriverRef->driver.print_buffer = &d_ttyDriver_print_buffer_stub;
}

void d_ttyDriver_destroy(void *self)
{
    free((ttyRef*)self);
}

void d_ttyDriver_print_char_stub(char ch)
{
    // Does nothing, since this is a stub.
}

void d_ttyDriver_print_buffer_stub(char *buf)
{
    // Does nothing, since this is a stub.
}
